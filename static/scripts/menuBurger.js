// -------------------------------------------MOBILE
console.log("hello");

let menuNavigationMobile = document.getElementById("menuMobile");
let recupSpanHaut = document.getElementById("spanHaut");
let recupSpanMilieu = document.getElementById("spanMilieu");
let recupSpanBas = document.getElementById("spanBas");

//MISE EN PLACE DU MENU BURGER
document.getElementById("burger").addEventListener("click", menuBurgerOuvert);

function menuBurgerOuvert() {
    document.getElementById("menuMobile").style.display = "flex";
    document.getElementById("burger").removeEventListener("click", menuBurgerOuvert);
    document.getElementById("burger").addEventListener("click", menuBurgerFerme);
    recupSpanHaut.style.transform = "translateY(12px) rotate(45deg) scale(1.5)";
    recupSpanMilieu.style.display = "none"
    recupSpanBas.style.transform = "translateY(-1px) rotate(-45deg) scale(1.5)"
}

function menuBurgerFerme() {
    document.getElementById("menuMobile").style.display = "none";
    document.getElementById("burger").removeEventListener("click", menuBurgerFerme);
    document.getElementById("burger").addEventListener("click", menuBurgerOuvert);
    recupSpanHaut.style.transform = "translateY(0px) rotate(0deg)";
    recupSpanMilieu.style.display = "block"
    recupSpanBas.style.transform = "translateY(0px) rotate(0deg)";
}